<?php

namespace Document\Request\Category;

use Document\Models\DocumentCategory;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => ['required'],
            'description' => ['nullable'],
            'url' => ['required', Rule::unique(DocumentCategory::class)->ignore($this->id)],
            'seo_title' => ['nullable'],
            'seo_description' => ['nullable'],
            'seo_keywords' => ['nullable'],
            'parent' => ['nullable'],
            'origin_name' => ['required','boolean'],
            'order' => ['nullable'],
            'access' => ['required']
        ];
    }

    protected function prepareForValidation()
    {
        if($this->parent == "on")
        {
            $this->merge([
                'parent' => null
            ]);
        }
        $this->merge([
            'origin_name' => $this->origin_name == "on"
        ]);
        if (!$this->url) {
            $this->merge([
                'url' => str_slug_new($this->title)
            ]);
        }
        if (!$this->seo_title) {
            $this->merge([
                'seo_title' => $this->title
            ]);
        }
    }

    public function authorize(): bool
    {
        return true;
    }
}
