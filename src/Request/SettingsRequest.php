<?php

namespace Document\Request;

use Illuminate\Foundation\Http\FormRequest;

class SettingsRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title'=>'nullable',
            'paginate'=>'nullable',
            'url'=>'nullable',
            'description'=>'nullable',
            'seo_title'=>'nullable',
            'seo_keywords'=>'nullable',
            'seo_description'=>'nullable',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}