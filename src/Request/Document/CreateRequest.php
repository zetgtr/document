<?php

namespace Document\Request\Document;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;

class CreateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title'=>['nullable','string'],
            'description'=>['nullable','string'],
            'file'=>['required','file'],
            'created_at'=>['nullable'],
            'category'=>['nullable'],
        ];
    }
    protected function prepareForValidation()
    {
        if($this->category == 'on')
            $this->merge([
                'category' => null
            ]);
        if(!$this->title && $this->file('file')){
            $this->merge([
                'title' => $this->file('file')->getClientOriginalName()
            ]);
        }
        if(!$this->created_at){
            $this->merge([
                'created_at' => Carbon::now()
            ]);
        }
    }

    public function authorize(): bool
    {
        return true;
    }
}
