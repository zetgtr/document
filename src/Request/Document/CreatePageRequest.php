<?php

namespace Document\Request\Document;

use Illuminate\Foundation\Http\FormRequest;

class CreatePageRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'files'=>['required','array'],
            'files.*'=> ['file'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
