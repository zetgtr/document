<?php

namespace Document\Request\Document;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title'=>['required','string'],
            'description'=>['nullable','string'],
            'file'=>['nullable','file'],
            'created_at'=>['nullable'],
            'category'=>['nullable'],
        ];
    }
    protected function prepareForValidation()
    {
        if($this->category == 'on')
            $this->merge([
                'category' => null
            ]);
    }

    public function authorize(): bool
    {
        return true;
    }
}
