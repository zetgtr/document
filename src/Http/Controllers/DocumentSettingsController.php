<?php

namespace Document\Http\Controllers;

use App\Http\Controllers\Controller;
use Catalog\Requests\Settings\UpdateRequest;
use Catalog\Models\Settings;
use Catalog\QueryBuilder\CatalogBuilder;
use Document\Enums\DocumentEnums;
use Document\Models\DocumentSettings;
use Document\QueryBuilder\DocumentBuilder;
use Document\Request\SettingsRequest;
use Illuminate\Http\Request;


class DocumentSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(DocumentBuilder $builder)
    {
        return view("document::document.admin.settings.index",['links'=>$builder->getNavigationLinks(DocumentEnums::SETTINGS->value)]);
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(SettingsRequest $request)
    {
        forEach($request->validated() as $key=>$value)
        {
            DocumentSettings::query()->updateOrCreate(['type'=>$key],['value'=>$value]);
        }
        return back()->with('success','Настройки успешно сохранены');
    }
}
