<?php

namespace Document\Http\Controllers;

use App\Http\Controllers\Controller;
use Catalog\QueryBuilder\CatalogBuilder;
use Document\Enums\CategoryEnums;
use Document\Enums\DocumentEnums;
use Document\Models\Document;
use Document\Models\DocumentCategory;
use Document\Models\DocumentSettings;
use Document\QueryBuilder\DocumentBuilder;
use Document\Request\Category\CreateRequest;
use Document\Request\Category\UpdateRequest;
use Document\Request\SettingsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class DocumentCategoryController extends Controller
{
    public function index(DocumentBuilder $builder){
        return view('document::document.admin.category.index',[
            'links'=>$builder->getNavigationLinks(DocumentEnums::CATEGORY->value),
            'categories' => $builder->getCategory()
        ]);
    }

    public function getDocument(string $document){
        $documentModel = Document::where('path','LIKE', "%".$document."%")->first();
        if($documentModel) {
            $file = Storage::disk('public')->path($documentModel->path);
            $headers = ['Content-Disposition' => 'attachment']; // По умолчанию настройка заголовка для скачивания
    
            // Если файл PDF, изменяем заголовок для отображения в браузере
            if (pathinfo($file, PATHINFO_EXTENSION) === 'pdf') {
                $headers['Content-Disposition'] = 'inline'; // Отображать в браузере
                $headers['Content-Type'] = 'application/pdf'; // Указываем тип содержимого
            }
    
            return response()->file($file, $headers);
        }
        return redirect()->back();
    }

    public function order(Request $request, DocumentBuilder $catalogBuilder){
        $catalogBuilder->setOrderCategory($request->all()['items']);
    }

    public function publish(DocumentCategory $category)
    {
        $category->publish = !$category->publish;
        if ($category->save()) return ['status' => true, 'publish' => $category->publish];
        else  return ['status' => false];
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create(DocumentBuilder $builder)
    {
        return view('document::document.admin.category.create',[
            'links' => $builder->getNavigationLinks(DocumentEnums::CATEGORY->value),
            'navigation' => $builder->getNavigationPageLink(CategoryEnums::CONTENT->value)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateRequest $request)
    {
        $category = DocumentCategory::create($request->validated());
        if ($category) {
            return \redirect()->route('admin.document.category.edit',['category'=>$category])->with('success', __('messages.admin.document.category.store.success'));
        }

        return \redirect()->route('admin.document.category.create')->with('error', __('messages.admin.document.category.store.fail'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(DocumentCategory $category,DocumentBuilder $builder)
    {
        return view('document::document.admin.category.edit',[
            'links' => $builder->getNavigationLinks(DocumentEnums::CATEGORY->value),
            'navigation' => $builder->getNavigationPageLink(CategoryEnums::CONTENT->value),
            'category' => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, DocumentCategory $category)
    {
        $category = $category->fill($request->validated());
        if ($category->save()) {
            return \back()->with('success', __('messages.admin.document.category.update.success'));
        }

        return \back()->with('error', __('messages.admin.document.category.update.fail'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(DocumentCategory $category)
    {
        try {
            $category->delete();
            $response = ['status' => true,'message' => __('messages.admin.document.category.destroy.success')];
        } catch (\Exception $exception)
        {
            $response = ['status' => false,'message' => __('messages.admin.document.category.destroy.fail').$exception->getMessage()];
        }

        return $response;
    }
}
