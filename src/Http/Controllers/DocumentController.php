<?php

namespace Document\Http\Controllers;

use App\Http\Controllers\Controller;
use Document\Enums\DocumentEnums;
use Document\Models\Document;
use Document\Models\DocumentCategory;
use Document\Models\DocumentSettings;
use Document\QueryBuilder\DocumentBuilder;
use Document\Request\Document\CreatePageRequest;
use Document\Request\Document\CreateRequest;
use Document\Request\Document\UpdateRequest;
use Document\Request\SettingsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;


class DocumentController extends Controller
{
    public function __construct(DocumentBuilder $builder)
    {
        View::share('links',$builder->getNavigationLinks(DocumentEnums::DOCUMENT->value));
        $this->path = '/storage/';
    }

    public function index(){
        return view('document::document.admin.document.index',['category'=>null]);
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create(DocumentBuilder $builder)
    {
        return view('document::document.admin.document.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateRequest $request ,DocumentBuilder $builder)
    {
        $category = DocumentCategory::find($request->category);
        $file = $request->file('file');
        if($category && $category->origin_name) {
            $path = 'document/' . $file->getClientOriginalName();
            if (Storage::disk('public')->exists($path)) {
                return back()->with('error', 'Файл с таким именем уже существует');
            }
        }
        $document = $builder->setDocument($request,$this->path,$category);
        if($document->save()) {
            $document->category()->attach($request->category);
            return redirect()->route('admin.document.index')->with('success', 'Документ успешно создан');
        }
        return back()->with('error','Ошибка создания документа');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id,DocumentBuilder $builder)
    {
        $category = DocumentCategory::find($id);
        return view('document::document.admin.document.index',['category'=>$category,'breadcrumb'=>$builder->getProductBreadcrumb($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Document $document)
    {
        return view('document::document.admin.document.edit',['document'=>$document]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Document $document)
    {
        $category = DocumentCategory::find($request->category);
        $file = $request->file('file');
        if($category && $category->origin_name && $file) {
            $path = 'document/' . $file->getClientOriginalName();
            if (Storage::disk('public')->exists($path)) {
                return back()->with('error', 'Файл с таким именем уже существует');
            }
        }

        if($file){
            $extension = $file->getClientOriginalExtension();
            if(Storage::disk('public')->exists($document->path))
                Storage::disk('public')->delete($document->path);
            if($category && $category->origin_name) {
                $path = Storage::disk('public')->putFileAs('document', $file, $file->getClientOriginalName());
            }
            else
                $path = Storage::disk('public')->put('document', $file);
            $document->link = $this->path.$path;
            $document->path = $path;
            $document->extension = $extension;
        }
        $document->title = $request->title;
        $document->description = $request->description;
        $document->created_at = $request->created_at;
        if ($document->save()) {
            $document->category()->detach();
            $document->category()->attach($request->category);
            return \redirect()->back()->with('success', 'Документ успешно обновлен');
        }

        return \back()->with('error', 'Ошибка обновления документа');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Document $document)
    {
        try {
            if(Storage::disk('public')->exists($document->path))
                Storage::disk('public')->delete($document->path);
            $document->delete();
            $response = ['status' => true,'message' => __('messages.admin.catalog.product.destroy.success')];
        } catch (Exception $exception)
        {
            $response = ['status' => false,'message' => __('messages.admin.catalog.product.destroy.fail').$exception->getMessage()];
        }

        return $response;

    }

    public function order(Request $request){
        foreach ($request->all()['items'] as $key=>$item)
        {
            $document = Document::find($item['id']);
            $document->order = $key;
            $document->save();
        }
    }

    public function getDocument(?DocumentCategory $category = null){
        $builder = new DocumentBuilder();
        if($category) {
            $categories = $category->children;
            $documents = $builder->getDocumentsCategory($category);
        } else {
            $categories = $builder->getCategory();
            $documents = $builder->getDocuments();
        }
        return ['status'=>true,'categories'=>$categories,'documents'=>$documents];
    }

    public function publish(Document $document){
        $document->publish = !$document->publish;
        $document->save();
        return ['status' => true, 'publish' =>  $document->publish];
    }

    public function crateDocument(CreatePageRequest $request){
        $files = $request->file('files');
        $data = [];
        $category = DocumentCategory::query()->updateOrCreate(['title'=>'Документы на стрницах'],['publish'=>false,'url'=>"",'page'=>true]);
        foreach ($files as $file){
            $extension = $file->getClientOriginalExtension();
            $document = new Document();
            $document->extension = $extension;
            $document->title = $file->getClientOriginalName();
            $path = Storage::disk('public')->put('document', $file);
            $document->link = $this->path.$path;
            $document->publish = true;
            $document->path = $path;
            if($document->save()){
                $document->category()->attach($category);
                $data[] = $document;
            }
        }
        return $data;
    }

   public function deleteDocument(Document $document){
            try {
                if(Storage::disk('public')->exists($document->path))
                    Storage::disk('public')->delete($document->path);
                $document->delete();
                $response = ['status' => true];
            } catch (\Exception $exception)
            {
                $response = ['status' => false];
            }

        return ['status' => true];
    }
}
