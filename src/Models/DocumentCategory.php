<?php

namespace Document\Models;

use Catalog\Models\Product;
use Illuminate\Database\Eloquent\Model;

class DocumentCategory extends Model
{
    protected $fillable = [
        'id',
        'title',
        'description',
        'url',
        'seo_title',
        'seo_description',
        'seo_keywords',
        'parent',
        'order',
        'publish',
        'origin_name',
        'page',
        'access'
    ];

    public function documents()
    {
        return $this->belongsToMany(Document::class, 'document_has_category','category_id','document_id','id','id')->orderBy('order');
    }
    public function childrenQuery()
    {
        return $this->hasMany(DocumentCategory::class, 'parent','id')->orderBy('order');
    }

    public function documentsOnPublish()
    {
        return $this->belongsToMany(Document::class, 'document_has_category','category_id','document_id','id','id')->where('publish', true)->orderBy('order');
    }

    public function getChildrenAttribute(){
        return DocumentCategory::where('parent',$this->attributes['id'])->orderBy('order')->get();
    }

    public function parentCategory(){
        return $this->belongsTo(DocumentCategory::class,'parent','id')->orderBy('order');
    }
}
