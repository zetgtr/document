<?php

namespace Document\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentSettings extends Model
{
    protected $fillable = [
        'type',
        'value'
    ];
}
