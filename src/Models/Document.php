<?php

namespace Document\Models;


use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = ['title','description','order','path','publish','link'];

    public function getLinkAttribute()
    {
        $category = $this->category->first();

        if(!$category || !$category->url)
            return $this->attributes['link'];
        $name = pathinfo($this->attributes['link'])['basename'];
        
        return $this->setUrl($category) ."/". $name;
    }

    private function setUrl($category,$url =""){
        $url = $url."/".$category->url;
        if($category->parentCategory)
            $url = $this->setUrl($category->parentCategory,$url ="");
        return $url;
    }
    public function category(){
        return $this->belongsToMany(DocumentCategory::class,'document_has_category','document_id','category_id','id','id');
    }
}
