<?php

namespace Document\View\Admin\Category\Edit;

use Closure;
use Document\Models\DocumentSettings;
use Document\QueryBuilder\DocumentBuilder;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Content extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(DocumentBuilder $builder,$category)
    {
        $this->categories = $builder->getCategoryAll();
        $this->category = $category;
    }
    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('document::components.admin.category.edit.content',['categories'=>$this->categories,'category'=>$this->category]);
    }
}
