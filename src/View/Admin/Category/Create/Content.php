<?php

namespace Document\View\Admin\Category\Create;

use Closure;
use Document\Models\DocumentCategory;
use Document\Models\DocumentSettings;
use Document\QueryBuilder\DocumentBuilder;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Content extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(DocumentBuilder $builder)
    {
        
        $this->categories = $builder->getCategoryAll();
    }
    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('document::components.admin.category.create.content',['categories'=>$this->categories]);
    }
}
