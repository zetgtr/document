<?php

namespace Document\View\Admin;

use Closure;
use Document\Models\DocumentSettings;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;
use Document\Models\Settings as Model;

class Settings extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct()
    {
        $this->title = DocumentSettings::where('type','title')->value('value');
        $this->paginate = DocumentSettings::where('type','paginate')->value('value');
        $this->url = DocumentSettings::where('type','url')->value('value');
        $this->description = DocumentSettings::where('type','description')->value('value');
        $this->seo_title = DocumentSettings::where('type','seo_title')->value('value');
        $this->seo_keywords = DocumentSettings::where('type','seo_keywords')->value('value');
        $this->seo_description = DocumentSettings::where('type','seo_description')->value('value');
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('document::components.admin.settings',[
                'title'=>$this->title,
                'paginate'=>$this->paginate,
                'url'=>$this->url,
                'description'=>$this->description,
                'seo_title'=>$this->seo_title,
                'seo_keywords'=>$this->seo_keywords,
                'seo_description'=>$this->seo_description
            ]);
    }
}
