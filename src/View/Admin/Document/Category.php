<?php

namespace Document\View\Admin\Document;

use Document\Models\DocumentCategory;
use Document\QueryBuilder\DocumentBuilder;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Request;
use Illuminate\View\Component;

class Category extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(DocumentBuilder $builder,$checked = null)
    {
        $this->categories = $builder->getCategoryAll();
        $category = null;
        if(request()->get('category')){
            $category = DocumentCategory::find(request()->get('category'));
        }
        $this->checked = $checked ?? $category;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('document::components.admin.document.category',['categories'=>$this->categories,'checked'=>$this->checked]);
    }
}
