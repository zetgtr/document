<?php

namespace Document\View\Admin\Document;

use Closure;
use Document\Models\DocumentSettings;
use Document\QueryBuilder\DocumentBuilder;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;
use Document\Models\Settings as Model;

class Index extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(DocumentBuilder $builder,$category = null)
    {

        if($category) {
            $this->categories = $category->children ?? [];
            $this->documents = $builder->getDocumentsCategory($category) ?? [];
        } else {
            $this->categories = $builder->getCategory() ?? [];
            $this->documents = $builder->getDocuments() ?? [];
        }

    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('document::components.admin.document.index',['categories'=>$this->categories,'documents'=>$this->documents]);
    }
}
