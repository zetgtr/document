<?php

namespace Document\QueryBuilder;

use Catalog\Models\Category;
use Catalog\Models\Order;
use Catalog\Models\Product;
use Catalog\Models\Settings;
use Document\Models\Document;
use Document\Models\DocumentCategory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;

class DocumentBuilder
{
    public function __construct()
    {
        $this->category = DocumentCategory::query();
    }

    public function getNavigationLinks($key = null)
    {
        $links = config('document.menu_links');
        if($key)
            $links[$key]['active'] = true;
        return $links;
    }

    public function setDocument($request,$pathDocument,$category)
    {
        $file = $request->file('file');
        $extension = $file->getClientOriginalExtension();
        $document = new Document();
        $document->title = $request->title;
        $document->description = $request->description;
        $document->created_at = $request->created_at;
        $document->extension = $extension;
        if($category && $category->origin_name) {
            $path = Storage::disk('public')->putFileAs('document', $file, $file->getClientOriginalName());
        }
        else
            $path = Storage::disk('public')->put('document', $file);
        $document->link = $pathDocument.$path;
        $document->path = $path;

        return $document;
    }

    public function getNavigationPageLink($key)
    {
        $links = config('document.category_links');
        if($key)
            $links[$key]['active'] = true;
        return $links;
    }

    public function setOrderCategory(array $items, int $parent = null)
    {
        foreach ($items as $key=>$item)
        {
            $this->category->where('id',$item['id'])->update(['parent'=>$parent,'order'=>$key]);
            $this->category = DocumentCategory::query();
            if(!empty($item['children']))
            {
                $this->setOrderCategory($item['children'], $item['id']);
            }
        }
    }

    public function getCategoryParent(): Collection
    {
        $category = $this->category->where('parent','=',null)->orderBy('order')->get();
        return $this->setCategoryParent($category);
    }

    private function setCategoryParent(Collection $items)
    {
        foreach ($items as $item)
        {
            $this->model = DocumentCategory::query();
            $parent = $this->model->where('parent','=',$item->id)->orderBy('order')->get();
            if (count($parent) > 0) {
                $item->parent = $parent;
                $this->setCategoryParent($item->parent);
            }
        }

        return $items;
    }

    public function getCategoryAll()
    {
        return $this->category->get();
    }

    public function getCategory(){
        return $this->getCategoryParent();
    }

    public function getDocuments(){
        return Document::whereDoesntHave('category')->orderBy('order')->get();
    }
    public function getDocumentsCategory($category){
        return Document::whereHas('category',function($q) use ($category){
            $q->where('category_id',$category->id);
        })->orderBy('order')->get();
    }

    public function setDocumentCode()
    {
        $filePath = resource_path('views/components/admin/page/page-service.blade.php');
        $fileContents = file_get_contents($filePath);
        $newButton = '<button class="btn btn-sm btn-success" type="button" id="add_document">Дбавить документы</button>'.PHP_EOL;
        $contents = str_replace($newButton,"",$fileContents);
        $contents = str_replace('<button class="btn btn-sm btn-dark" type="button" id="datahub_add_navigation">', $newButton . '<button class="btn btn-sm btn-dark" type="button" id="datahub_add_navigation">', $contents);
        $script = PHP_EOL."@vite('resources/js/document/page.js')";

        $contents = str_replace($script,"",$contents);
        $contents = $contents.$script;
        $modal = PHP_EOL."<x-document::modal.page />";
        $contents = str_replace($modal,"",$contents);
        $contents = $contents.$modal;
//        $hidden = PHP_EOL."<input type='hidden' id='document_value' />";
//        $contents = str_replace($hidden,"",$contents);
//        $contents = $contents.$hidden;
        file_put_contents($filePath, $contents);
    }


    public function getProductBreadcrumb(string $categoryId)
    {
        $category = DocumentCategory::find($categoryId);
        $breadcrumbs = [];

        $breadcrumbs[] = [
            'title' => $category->title,
            'url' => route('admin.document.show', $category->id)
        ];

        if ($category->parentCategory) {
            $parentBreadcrumbs = $this->getProductBreadcrumb($category->parentCategory->id);
            $breadcrumbs = array_merge($parentBreadcrumbs, $breadcrumbs);
        }

        return $breadcrumbs;

    }
}
