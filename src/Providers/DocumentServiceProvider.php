<?php

namespace Document\Providers;

use App\Actions\Fortify\CreateNewUser;
use App\Actions\Fortify\ResetUserPassword;
use App\Actions\Fortify\UpdateUserPassword;
use App\Actions\Fortify\UpdateUserProfileInformation;
use App\QueryBuilder\QueryBuilder;
use App\QueryBuilder\RolesBuilder;
use Document\QueryBuilder\DocumentBuilder;
use Document\View\Admin\Category\Create\Content;
use Document\View\Admin\Category\Edit\Content as EditContent;
use Document\View\Admin\Document\Category;
use Document\View\Admin\Document\Index;
use Document\View\Admin\Settings;
use Illuminate\Support\ServiceProvider;
use Laravel\Fortify\Fortify;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\AliasLoader;
use Document\Http\Middleware\Auth as PacageAuth;
use Illuminate\View\Middleware\ShareErrorsFromSession;

class DocumentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
        }

        $this->publishes([
            __DIR__ . '/../../database/migrations' => database_path('migrations'),
        ], 'migrations');

        $this->app['router']->aliasMiddleware('auth_pacage', PacageAuth::class);
        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'document');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/api.php');
        $this->components();
    }

    private function components()
    {
        Blade::component(Settings::class, 'document::admin.settings');
        Blade::component(Content::class, 'document::admin.category.create.content');
        Blade::component(EditContent::class, 'document::admin.category.edit.content');
        Blade::component(Index::class, 'document::admin.document.index');
        Blade::component(Category::class, 'document::admin.document.category');
    }

    private function singletons()
    {
        $this->app->singleton(Category::class, function ($app, $parameters) {
            $builder = $app->make(DocumentBuilder::class);
            $checked = $parameters['checked'];
            return new Category($builder, $checked);
        });
        $this->app->singleton(Index::class, function ($app, $parameters) {
            $builder = $app->make(DocumentBuilder::class);
            $category = $parameters['category'];
            return new Index($builder, $category);
        });
    }
    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(QueryBuilder::class, DocumentBuilder::class);
        $this->mergeConfigFrom(__DIR__.'/../../config/document.php', 'document');
        $this->singletons();
    }
}
