<?php

namespace Document\Enums;

enum DocumentEnums: string
{
    case DOCUMENT = 'document';
    case CATEGORY = 'category';
    case SETTINGS = 'settings';
}
