<?php

namespace Document\Seeders;

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \DB::table('menus')->insert($this->getData());
    }

    public function getData()
    {
        return [
            ['id'=>996,'name'=>'Документы','position'=>'left','logo'=>'fal fa-books','controller'=>'Document\Http\Controllers\DocumentController','url'=>'document','parent'=>5],
        ];
    }
}
