<?php

namespace Document\Seeders;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            MenuSeeder::class
        ]);
    }
}
