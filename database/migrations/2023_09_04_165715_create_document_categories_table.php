<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\AccessEnums;

class CreateDocumentCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('document_categories', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('url');
            $table->string('seo_title')->nullable();
            $table->text('seo_description')->nullable();
            $table->text('seo_keywords')->nullable();
            $table->integer('parent')->nullable();
            $table->integer('order')->default(0);
            $table->integer('publish')->default(1);
            $table->enum('access',[AccessEnums::all()]);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('document_categories');
    }
}
