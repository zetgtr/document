<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('document_has_category', function (Blueprint $table) {
            $table->foreignId('document_id')
                ->references('id')->on('documents')
                ->cascadeOnDelete();
            $table->foreignId('category_id')->nullable()
                ->references('id')->on('document_categories')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('document_has_cotegory');
    }
};
