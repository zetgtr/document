<?php

use Laravel\Fortify\Features;
use Document\Enums\DocumentEnums;
use Document\Enums\CategoryEnums;

return [
    'guard' => 'web',
    'middleware' => ['web'],
    'auth_middleware' => 'auth',
    'passwords' => 'users',
    'username' => 'email',
    'email' => 'email',
    'views' => true,
    'home' => '/home',
    'prefix' => '',
    'domain' => null,
    'limiters' => [
        'login' => null,
    ],
    'menu_links' => [
        DocumentEnums::DOCUMENT->value => ['route' => 'admin.document.index', 'name' => 'Документы'],
        DocumentEnums::CATEGORY->value => ['route' => 'admin.document.category.index', 'name' => 'Категории'],
        DocumentEnums::SETTINGS->value => ['route' => 'admin.document.settings.index', 'name' => 'Настройки'],
    ],
    'category_links' => [
        CategoryEnums::CONTENT->value => ['url' => CategoryEnums::CONTENT->value, 'name' => 'Контент'],
        CategoryEnums::SEO->value => ['url' => CategoryEnums::SEO->value, 'name' => 'SEO']
    ],
    'redirects' => [
        'login' => null,
        'logout' => null,
        'password-confirmation' => null,
        'register' => null,
        'email-verification' => null,
        'password-reset' => null,
    ],
    'features' => [
        Features::registration(),
        Features::resetPasswords(),
        Features::emailVerification(),
        Features::updateProfileInformation(),
        Features::updatePasswords(),
        Features::twoFactorAuthentication(),
    ],
];
