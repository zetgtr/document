<?php

use Illuminate\Support\Facades\Route;
use Document\Http\Controllers\DocumentController;
use Document\Http\Controllers\DocumentCategoryController;
use Document\Http\Controllers\DocumentSettingsController;



Route::middleware('web')->group(function (){
    Route::middleware('auth_pacage')->group(function () {
        Route::group(['prefix'=>"admin", 'as'=>'admin.', 'middleware' => 'is_admin'],static function(){
            Route::resource('document',DocumentController::class);
            Route::group(['as'=>'document.','prefix'=>"documents"],static function(){
                Route::resource('category',DocumentCategoryController::class);
                Route::get('publish/{document}',[DocumentController::class,'publish'])->name('publish');
                Route::post('order',[DocumentController::class,'order'])->name('order');
                Route::post('category/order',[DocumentCategoryController::class,'order'])->name('category.order');
                Route::get('category/publish/{category}',[DocumentCategoryController::class,'publish'])->name('category.publish');
                Route::resource('settings',DocumentSettingsController::class);
            });
        });
    });
    try {
        $builder = new \Document\QueryBuilder\DocumentBuilder();
        $categories = $builder->getCategory();
        function setRouteDocument($categories,$url = ""){
            foreach ($categories as $category){
                if($category->url) {
                    $url = $url . "/" . $category->url;
                    Route::get($category->url . '/{document}', [DocumentCategoryController::class, 'getDocument']);
                    if ($category->parent instanceof Illuminate\Support\Collection)
                        setRouteDocument($category->parent, $url);
                }
            }
        }
        setRouteDocument($categories);
    } catch (Exception $exception) {
        
    }
});
