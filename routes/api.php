<?php

use Illuminate\Support\Facades\Route;
use Document\Http\Controllers\DocumentController;

Route::middleware('api')->group(function () {
    Route::group(['prefix' => "api"], static function () {
        Route::group(['prefix' => 'document'], static function (){
            Route::get('/',[DocumentController::class,'getDocument']);
            Route::get('/{category}',[DocumentController::class,'getDocument']);
            Route::post('/create',[DocumentController::class,'crateDocument']);
            Route::delete('/remove/{document}',[DocumentController::class,'deleteDocument']);
        });
    });
});
