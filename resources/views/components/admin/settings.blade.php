<form action="{{ route('admin.document.settings.store') }}" method="POST" class="row">
    @csrf
    <div class="col-lg-6">
        <div class="row">
            <div class="form-group col-9">
                <label for="title">Заголовок</label>
                <input type="text" name="title" id="title" class="form-control" value="{{ old('title',$title ?? "") }}">
                <x-error errorValue="title" />
            </div>
            <div class="form-group col-3">
                <label for="paginate">Пагинация</label>
                <input type="text" name="paginate" id="paginate" class="form-control" value="{{ old('paginate',$paginate ?? "") }}">
                <x-error errorValue="paginate" />
            </div>
        </div>

        <div class="form-group">
            <label for="url">Url</label>
            <input type="text" name="url" id="url" class="form-control" value="{{ old('url',$url ?? "") }}">
            <x-error errorValue="url" />
        </div>
        <div class="form-group">
            <label for="description">Описание</label>
            <textarea name="description" id="my-editor" class="form-control @error('description') is-invalid @enderror my-editor">{{ old('description',$description ?? "") }}</textarea>
            <x-error errorValue="description" />
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <label for="seo_title">SEO Title</label>
            <input type="text" name="seo_title" id="seo_title" class="form-control" value="{{ old('seo_title',$seo_title ?? "") }}">
            <x-error errorValue="seo_title" />
        </div>
        <div class="form-group">
            <label for="seo_keywords">SEO Keywords</label>
            <input type="text" name="seo_keywords" id="seo_keywords" class="form-control @error('seo_keywords') is-invalid @enderror" value="{{ old('seo_keywords',$seo_keywords ?? "") }}">
            <x-error errorValue="seo_keywords" />
        </div>
        <div class="form-group">
            <label for="seo_description">SEO Description</label>
            <textarea name="seo_description" style="height: 400px;" id="seo_description" class="form-control @error('seo_description') is-invalid @enderror">{{ old('seo_description',$seo_description ?? "") }}</textarea>
            <x-error errorValue="seo_keywords" />
        </div>
    </div>
    <div class="col-lg-12">
        <input type="submit" value="Сохранить" class="btn btn-sm btn-success">
    </div>
</form>
