@if(!empty($documents) && count($documents) > 0)
    <ol class="dd-list">
        @foreach($documents as $item)
            <li class="dd-item delete-element" data-id="{{$item->id}}" data-name="{{$item->title}}">
                <div class="dd-handle">
                    <span>{{$item->title}}</span>
                </div>
                <div class="dnd-edit show-dnd">
                    <a href="/api/document/remove/{{$item->id}}" class="button-delete delete btn btn-danger btn-xs pull-right"
                       data-owner-id="1">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </a>
                    <a href="{{route('admin.document.edit', ['document'=>$item])}}" class="button-edit btn btn-warning btn-xs pull-right"
                       data-owner-id="1">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
                    @if($item->publish)
                        <a href="{{ route('admin.document.publish', ['document'=>$item]) }}" class="btn btn-success show-publish">
                            <i class="far fa-eye"></i>
                        </a>
                    @else
                        <a href="{{ route('admin.document.publish', ['document'=>$item]) }}" class="btn btn-default show-publish">
                            <i class="far fa-eye-slash"></i>
                        </a>
                    @endif
                </div>
            </li>
        @endforeach
    </ol>
@else
    <span>Документы отсутствуют</span>
@endif
<script src="{{asset('assets/js/admin/delete.js')}}" ></script>
<script src="{{ asset('assets/js/admin/show.js') }}"></script>
