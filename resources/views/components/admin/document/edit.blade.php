<form class="row" action="{{ route('admin.document.update',['document'=>$document]) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    <div class="col-lg-8 row">
        <div class="col-lg-6">
            <div class="form-group">
                <label>Название документа</label>
                <input type="text" name="title" class="form-control @error('title')is-invalid @enderror" value="{{ old('title',$document->title) }}">
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label>Документ</label>
                <input type="file" name="file" class="form-control @error('file')is-invalid @enderror">
            </div>

            <a href="{{ $document->link }}">{{ $document->title }}</a>
        </div>
        <div class="col-lg-12">
            <div class="form-group">
                <label>Описание документа</label>
                <textarea name="description" id="my-editor" class="form-control @error('description') is-invalid @enderror my-editor">{{ old('description',$document->description) }}</textarea>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>Дата публикации</label>
                        <input type="text" data-language="ru" name="created_at" id="addDates" class="form-control @error('created_at')is-invalid @enderror" value="{{old('created_at',$document->created_at->format('d.m.Y H:i')) }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">

        <x-document::admin.document.category :checked="$document->category->first()" />
    </div>

    <div class="col-lg-12">
        <input type="submit" value="Сохранить" class="btn btn-sm btn-success">
    </div>
</form>
@vite('resources/js/utils/AirDatepicker.js')
