<style>
    .box_category {
        height: 485px;
        overflow: auto;
        border: 1px solid #e9edf4;
        padding-left: 10px;
        padding-top: 10px;
        padding-bottom: 10px;
    }
</style>

<div class="tab-content">
    <div class="iconed-caption">
        <i class="fas fa-link"></i>
        Родительская категория
    </div>

    <div class="mt-2 box_category">
        <div class="outlined-radio ">
            <input type="radio" name="category" @checked(!$checked) id="category0"
                   class="d-none" @checked(!old('category'))>
            <label class="d-block text-left" for="category0">Корневая директория</label>
        </div>
        @forelse($categories as $category)
            <div class="outlined-radio">
                <input id="category{{$category->id}}"
                       @checked(old('category',$checked ? $checked->id : false)==$category->id) type="radio"
                       class="d-none" value="{{$category->id}}" name="category">
                <label class="d-block text-left" for="category{{$category->id}}">{{$category->title}}</label>
            </div>
        @empty
            <div class="d-flex w-100 mt-3 justify-content-center">Не созданы</div>
        @endforelse
    </div>

    <div class="tree-subchildren">
    </div>
</div>
<link id="style" href="{{asset('assets/css/admin/catalog.css')}}" rel="stylesheet">
