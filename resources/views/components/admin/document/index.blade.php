<div class="row">
    <div class="col-lg-6">
        <label>Документы</label>
        <div class="dd nestable" data-max-depth="1" id="nestable">
            <x-document::admin.document.list :documents="$documents" />
        </div>
    </div>
    <div class="col-lg-6">
        <label>Категории</label>
        @forelse($categories as $category)

            <a href="{{ route('admin.document.show', ['document'=>$category->id]) }}">
                <div class="border-0 p-0 mb-2">
                    <div class="media mt-0 border">
                        <div class="ps-0 me-3">
                            <i class="fas fa-external-link-alt shared-files text-muted"></i>
                        </div>
                        <div class="media-body">
                            <div class="d-flex align-items-center">
                                <div class="mt-3">
                                    <h5 class="mb-1 fs-13 fw-semibold text-dark">{{ $category->title }}</h5>
                                </div>
                                <span class="mt-2 ms-auto fs-14">
                                <span class="float-end">
                                    <span class="op-7 text-muted">
                                        <i class="fas fa-external-link-alt"></i>
                                        Открыть
                                    </span>
                                </span>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        @empty
            Не созданы категории
        @endforelse
    </div>
</div>
<input type="hidden" id="route_dd" value="{{  route('admin.document.order') }}">
<script src="{{asset('assets/js/admin/dnd.js')}}" ></script>
