<div class="modal fade effect-fall" id="modaldemo8">
    <div class="modal-dialog modal-xl modal-dialog-centered text-center" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><i
                        class="fas fa-times"></i></button>
            </div>
            <div class="modal-body">
                <div class="modal-warning">

                </div>
                <div class="modal-wrapper"></div>
            </div>
            <div class="modal-footer">
                <div class="buttons-extends "></div>
                <div class="d-flex justify-content-between w-100">
                    <button type="button" class="btn btn-sm btn-warning modal-back" id="modal-back">Назад</button>
                    <button type="button" class="btn btn-sm btn-success modal-save">Сохранить</button>
                </div>

            </div>
        </div>
    </div>
</div>
<template id="modal_page">
    <div class="row">
        <div class="col-lg-6">
            <button class="btn btn-success btn-file">Вставить свои файлы</button>
        </div>
        <div class="col-lg-6">
            <button class="btn btn-warning btn-document">Выбрать файлы из документов</button>
        </div>
    </div>
</template>

<template id="modal_document">
    <div class="row">
        <div class="col-lg-6">
            <table class="table list_panel">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Документ</th>
                    <th scope="col" style="text-align: end">Вкл/Выкл</th>
                </tr>
                </thead>
                <tbody class="container_document">
                </tbody>
            </table>
        </div>
        <div class="col-lg-6">
            <label>Категории</label>
            <div class="dd nestable" data-max-depth="1" id="nestable">
                <ol class="dd-list container_category">

                </ol>
            </div>
        </div>
    </div>
</template>
<template id="modal_document_document">
    <tr class="panel-line" data-sel="">
        <td><span>1</span></td>
        <td>
            <div class="panel-line__control" style="text-align: end">
                <div class="btn btn-i btn-full"><i class="fas fa-check-circle"></i></div>
            </div>
        </td>
    </tr>
</template>

<template id="modal_document_category">
    <li class="dd-item delete-element" style="cursor: pointer" data-id="2" data-name="test2">
        <div class="dd-handle d-flex justify-content-between">
            <span></span>
            <input type="checkbox" id="checkbox_category" class="btn btn-success add">
        </div>
    </li>
</template>

<template id="modal_document_file">
    <form enctype="multipart/form-data" >
        <div class="form-group">
            <label>Загрузите файлы</label>
            <input type="file" multiple name="files[]" class="form-control">
        </div>
    </form>
</template>
<template id="modal_document_edit">
    <form >
        <div class="form-group">
            <label>Введите название</label>
            <input type="input" id="file_name" class="form-control">
        </div>
    </form>
</template>
