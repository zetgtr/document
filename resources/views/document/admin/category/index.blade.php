@extends('layouts.admin')
@section('title',"Документы")
@section('content')
    <div class="card">
        <x-admin.navigation :links="$links" />
        <div class="card-body">
            <div class="row row-page-create">
                <div class="col-md-8 order-md-first">
                    <div class="dd nestable" id="nestable">
                        <x-document::admin.category.list :categories="$categories" />
                    </div>
                </div>
                <div class="col-md-4 order-md-first d-flex align-items-start justify-content-end">
                    <a href="{{ route('admin.document.category.create') }}" class="btn btn-primary">Добавить категорию</a>
                </div>
            </div>

        </div>
    </div>
    <input type="hidden" id="route_dd" value="{{ route('admin.document.category.order') }}">
    <script src="{{ asset('assets/js/admin/dnd.js') }}"></script>
    <script src="{{ asset('assets/js/admin/delete.js') }}"></script>
    <script src="{{ asset('assets/js/admin/show.js') }}"></script>
@endsection
@section("breadcrumb")
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.document.index")}}">Документы</a></li>
            <li class="breadcrumb-item active" aria-current="page">Категории</li>
        </ol>
    </div>
@endsection
