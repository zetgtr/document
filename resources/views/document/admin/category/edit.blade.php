@extends('layouts.admin')
@section('title',"Каталог")
@section('content')
    <div class="card">
        <x-admin.navigation :links="$links" />
        <x-admin.navigatin-js :links="$navigation" />
        <div class="card-body">
            <x-warning />
            <form action="{{ route('admin.document.category.update',['category'=>$category]) }}" method="post" class="row row-page-create">
                @csrf
                @method('PUT')
                <input type="hidden" value="{{$category->id}}" name="id">
                <div class="tab-content">
                    <x-document::admin.category.edit.content :category="$category" />
                    <x-document::admin.category.edit.seo :category="$category" />
                </div>
                <div>
                    <input type="submit" value="Сохранить" class="btn btn-success btn-sm">
                </div>
            </form>
        </div>
    </div>
@endsection
@section("breadcrumb")
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.document.index")}}">Документы</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.document.category.index")}}">Категории</a></li>
            <li class="breadcrumb-item active" aria-current="page">Редактировать</li>
        </ol>
    </div>
@endsection