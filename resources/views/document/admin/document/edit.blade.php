@extends('layouts.admin')
@section('title',"Документы")
@section('content')
    <div class="card" style="position: relative">
        <x-admin.navigation :links="$links" />
        <div class="card-body">
            <x-warning />

            <x-document::admin.document.edit :document="$document" />
        </div>
    </div>
@endsection
@section("breadcrumb")
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.document.index")}}">Документы</a></li>
            <li class="breadcrumb-item active" aria-current="page">Редактировать</li>
        </ol>
    </div>
@endsection
