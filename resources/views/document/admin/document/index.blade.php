@extends('layouts.admin')
@section('title',"Документы")
@section('content')
    <div class="card" style="position: relative">
        <x-admin.navigation :links="$links" />
        <div class="d-flex justify-content-between" style="position: absolute;right: 10px;top: 10px;">
            <div>
{{--                <a data-id="1" data-name="text" class="btn btn-sm search modal-effect btn-warning badge" data-bs-effect="effect-fall" data-bs-toggle="modal" data-bs-target="#modaldemo8" type="button">Поиск</a>--}}

                <a href="{{ route('admin.document.create',['category'=>$category]) }}" class="btn btn-sm btn-outline-primary">
                    <small>Добавить позицию</small>
                </a>
            </div>
        </div>
        <div class="card-body">
            <x-document::admin.document.index :category="$category" />
        </div>
    </div>
@endsection
@section("breadcrumb")
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.document.index")}}">Документы</a></li>
            @if(!empty($breadcrumb) && count($breadcrumb)>0)
                @foreach($breadcrumb as $item)
                    @if(!$loop->last)
                        <li class="breadcrumb-item"><a href="{{$item['url']}}">{{$item['title']}}</a></li>
                    @else
                        <li class="breadcrumb-item active" aria-current="page">{{$item['title']}}</li>
                    @endif
                @endforeach
            @endif
        </ol>
    </div>
@endsection
