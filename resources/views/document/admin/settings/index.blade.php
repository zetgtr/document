@extends('layouts.admin')
@section('title',"Документы")
@section('content')
    <div class="card">
        <x-admin.navigation :links="$links" />
        <div class="card-body">
            <x-document::admin.settings />
        </div>
    </div>
@endsection
@section("breadcrumb")
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.document.index")}}">Документы</a></li>
            <li class="breadcrumb-item active" aria-current="page">Настройки</li>
        </ol>
    </div>
@endsection
