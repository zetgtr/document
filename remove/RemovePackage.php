<?php

namespace Document\Remove;

use App\Models\Admin\Menu;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;



class RemovePackage
{
    private $pathMigration;
    private $pathScript;
    private $pathVues;
    public function __construct()
    {
        $this->pathMigration = database_path('migrations');
        $this->pathScript = public_path('assets/js/admin/document');
        $this->pathVues = resource_path('views/vendor/document');
    }

    private function deleteMigration($files){
        $check = false;
        foreach ($files as $file) {
            try {
                $path = $this->pathMigration."/".$file->getFilename();
                if (File::exists($path)) {
                    Artisan::call('migrate:rollback --path=database/migrations/'.$file->getFilename());
                    unlink($path);
                }
            }catch (\Exception $exception){
                $check = true;
            }
        }
        if($check){
            $this->deleteMigration($files);
        }
    }

    public function run($settings,$migration = false, $script = false, $vies = false)
    {
        chdir(base_path());
        if($migration)
        {
            $directory = __DIR__ . "/../database/migrations";
            $files = File::files($directory);
            $this->deleteMigration($files);

            $menu = Menu::query()->find(996);
            if($menu)
                $menu->delete();
        }

        if($script)
        {
            if (File::isDirectory($this->pathScript))
                File::deleteDirectory($this->pathScript);
        }

        if ($vies)
        {
            if (File::isDirectory($this->pathVues))
                File::deleteDirectory($this->pathVues);
        }
    }
}
